// Version provides binary version information.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package version

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Values to be overridden at compile by goreleaser or makefile.
var (
	// A string describing the semantic version and whether the repository is
	// dirty. Example output: "v1.2.3-dirty".
	//	git describe --tags --abbrev=0 --match="v*" --dirty
	gitDescribe = ""
	// The current git commit. This can be in short or long format.
	//	git rev-parse --short HEAD
	gitCommit = ""
	// The current git branch.
	//  git branch --show-current
	gitBranch = ""
	// The date in RFC-3339 format on which this build was made.
	//	date -u '+%Y-%m-%dT%TZ'
	buildDate = ""
)

// Info contains versioning information.
type Info struct {
	Major     int
	Minor     int
	Patch     int
	GitCommit string
	GitBranch string
	GitDirty  bool
	Date      time.Time
}

// info contains the parsed compile-time values.
var info Info

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// parseGitDescribe attempts to parse out a major, minor, and patch number from the semantic version number s, along with the dirty state.
func parseGitDescribe(s string) (int, int, int, bool, error) {
	// Lower-case the string and trim any white space
	s = strings.TrimSpace(strings.ToLower(s))
	// Split on the final hyphen, if any
	var dirty bool
	idx := strings.LastIndex(s, "-")
	if idx != -1 {
		dirty = s[idx:] == "-dirty"
		s = s[:idx]
	}
	// Strip off any leading 'v' and split on the period
	S := strings.Split(strings.TrimPrefix(s, "v"), ".")
	if len(S) != 3 {
		return 0, 0, 0, false, fmt.Errorf("expected 3 components but found %d", len(S))
	}
	// Parse the major version number
	major, err := strconv.Atoi(S[0])
	if err != nil {
		return 0, 0, 0, false, fmt.Errorf("unable to parse major version number: %w", err)
	} else if major < 0 {
		return 0, 0, 0, false, fmt.Errorf("invalid major version number: %d", major)
	}
	// Parse the minor version number
	minor, err := strconv.Atoi(S[1])
	if err != nil {
		return 0, 0, 0, false, fmt.Errorf("unable to parse minor version number: %w", err)
	} else if minor < 0 {
		return 0, 0, 0, false, fmt.Errorf("invalid minor version number: %d", minor)
	}
	// Parse the patch version number
	patch, err := strconv.Atoi(S[2])
	if err != nil {
		return 0, 0, 0, false, fmt.Errorf("unable to parse patch version number: %w", err)
	} else if patch < 0 {
		return 0, 0, 0, false, fmt.Errorf("invalid patch version number: %d", patch)
	}
	// Looks good
	return major, minor, patch, dirty, nil
}

// parseGitCommit performs a basic sanity check on the git commit hash s.
func parseGitCommit(s string) (string, error) {
	// Lower-case the string and trim any white space
	s = strings.TrimSpace(strings.ToLower(s))
	// Check the string length
	if len(s) < 4 || len(s) > 40 {
		return "", fmt.Errorf("invalid hash length: %d", len(s))
	}
	// Check that it only contains hex values
	if strings.IndexFunc(s, func(r rune) bool {
		return (r < '0' || r > '9') && (r < 'a' || r > 'f')
	}) != -1 {
		return "", errors.New("hash contains invalid characters")
	}
	// Looks good
	return s, nil
}

// parseGitBranch performs a basic sanity check on the git branch name s.
func parseGitBranch(s string) (string, error) {
	// Trim any white space. Note that git branch names are case-sensitive.
	s = strings.TrimSpace(s)
	// The branch name can't be empty
	if len(s) == 0 {
		return "", errors.New("invalid branch name")
	}
	// Looks good
	return s, nil
}

// init converts the compile-time values to Info.
func init() {
	// Parse the semantic version and dirty state
	if len(gitDescribe) != 0 {
		major, minor, patch, dirty, err := parseGitDescribe(gitDescribe)
		if err != nil {
			panic(fmt.Sprintf("malformed gitDescribe string (%s): %s", gitDescribe, err))
		}
		info.Major, info.Minor, info.Patch = major, minor, patch
		info.GitDirty = dirty
	}
	// Parse the git commit
	if len(gitCommit) != 0 {
		commit, err := parseGitCommit(gitCommit)
		if err != nil {
			panic(fmt.Sprintf("malformed gitCommit string (%s): %s", gitCommit, err))
		}
		info.GitCommit = commit
	}
	// Parse the git branch
	if len(gitBranch) != 0 {
		branch, err := parseGitBranch(gitBranch)
		if err != nil {
			panic(fmt.Sprintf("malformed gitBranch string (%s): %s", gitBranch, err))
		}
		info.GitBranch = branch
	}
	// Parse the build date
	if len(buildDate) != 0 {
		t, err := time.Parse(time.RFC3339, buildDate)
		if err != nil {
			panic(fmt.Sprintf("malformed buildDate (%s): %s", buildDate, err))
		}
		info.Date = t
	}
}

/////////////////////////////////////////////////////////////////////////
// Info functions
/////////////////////////////////////////////////////////////////////////

// Version returns the version as a string with leading "v".
func (i Info) Version() string {
	return fmt.Sprintf("v%d.%d.%d", i.Major, i.Minor, i.Patch)
}

// String returns a string summarising the version.
func (i Info) String() string {
	s := i.Version()
	if i.GitDirty || (i.Major == 0 && i.Minor == 0 && i.Patch == 0) {
		if len(i.GitCommit) != 0 {
			s += "-" + i.GitCommit
		}
		s += " (development version)"
	}
	if i.GitBranch != "" && i.GitBranch != "main" {
		s += " (branch: " + i.GitBranch + ")"
	}
	return s
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// GetInfo returns the known version info.
func GetInfo() Info {
	return info
}
