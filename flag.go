// Flag provides a standardised version flag.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package version

import (
	"fmt"
	"os"
	"runtime"
)

// Flag provides a standardised version flag.
type Flag struct {
	AppName string // The application name to use when printing the version
}

/////////////////////////////////////////////////////////////////////////
// Flag functions
/////////////////////////////////////////////////////////////////////////

// Name returns the flag name (without leading hyphen).
func (*Flag) Name() string {
	return "V"
}

// AlternativeName returns the alternative flag name (without leading hyphen).
func (*Flag) AlternativeName() string {
	return "version"
}

// Description returns a one-line description of this flag.
func (*Flag) Description() string {
	return "Display the version information and exit"
}

// Usage returns the empty string.
func (*Flag) Usage() string {
	return ""
}

// IsBoolean returns true.
func (*Flag) IsBoolean() bool {
	return true
}

// Parse outputs the version details to os.Stderr and exits.
func (f *Flag) Parse(_ string) error {
	// Output the app name and version info
	fmt.Fprintf(os.Stderr, "%s %s\n", f.AppName, info)
	// Output additional info about the build
	s := "Built using " + runtime.Version()
	if !info.Date.IsZero() {
		s += " on " + info.Date.Local().Format("Mon, 02 Jan 2006 at 15:04:05 MST")
	}
	fmt.Fprintf(os.Stderr, "%s.\n", s)
	// Exit the process
	os.Exit(0)
	return nil
}
